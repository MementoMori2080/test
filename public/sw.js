//Instalcion Service Worker
self.addEventListener('install',  (event) => {
    console.log('SW instalado')
   //self.skipWaiting()
    const instalacion = new Promise ((resolve, reject) => {
        setTimeout(() =>{
            console.log('Termino de la instalacion')
            self.skipWaiting()
            resolve()
        }, 1000)
    })
    event.waitUntil(instalacion)
})

//Activacion del Service Worker
self.addEventListener('activate',  (event) => {
    console.log('SW activado 2')
})

//Maneja las peticiones
self.addEventListener("fetch", function (event) {
    if (event.request.url.includes("https://fakestoreapi.com/products/1")) {
        const resp = new Response({"ok": false, "mensaje": "Este recurso no está disponible"});
        event.respondWith(resp);
    }

    console.log("SW fetch", event.request.url);
});